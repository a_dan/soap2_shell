<?php
class Shell_Soap2 
{
    const SERVICE_URL = '/api/v2_soap/?wsdl';
    const DEFAULT_FORMAT = 'xml';
    
    public $remoteUrl;
    public $apiUser;
    public $apiKey;
    public $from;
    public $to;
    
    public function __construct()
    {
        $this->_parseArgs();
        $this->_prepareArgs();
        $this->showHelp();
    }
    
    protected function _prepareArgs()
    {
        $arguments = array(
            'remoteUrl',
            'apiUser',
            'apiKey',
            'from',
            'to'
        );
        
        foreach ($arguments as $argument) {
            if (isset($this->_args[$argument])) {
                $this->{$argument} = $this->_args[$argument];
            }  
        }
    }
    
    protected function _parseArgs()
    {
        $current = null;
        foreach ($_SERVER['argv'] as $arg) {
            $match = array();
            if (preg_match('#^--([\w\d_-]{1,})$#', $arg, $match) || preg_match('#^-([\w\d_]{1,})$#', $arg, $match)) {
                $current = $match[1];
                $this->_args[$current] = true;
            } else {
                if ($current) {
                    $this->_args[$current] = $arg;
                } else if (preg_match('#^([\w\d_]{1,})$#', $arg, $match)) {
                    $this->_args[$match[1]] = true;
                }
            }
        }
        return $this;
    }
    
    protected function _argValidation()
    {
        return ($this->remoteUrl && $this->apiUser && $this->apiKey);
    }
    
    public function run()
    {
        if ($this->_argValidation()) {
            try {
                $filter = array();
                if ($this->from && $this->to) {
                    echo 'Date filter: from ' . $this->prepareDateForFilter($this->from) . ' to ' . $this->prepareDateForFilter($this->to) . "\n";
                    $filter = array('complex_filter' => array(
                        array('key' => 'created_at', 'value' => array('key' => 'from', 'value' => $this->prepareDateForFilter($this->from))),
                        array('key' => 'created_at', 'value' => array('key' => 'to', 'value' => $this->prepareDateForFilter($this->to))),
                    ));
                }
                $result = $this->getOrdersData($filter);
                print_r($result);
            } catch (Exception $e) {
                print_r($e);
            }
        } else {
            echo 'Required arguments are not defined'; 
        }
    }
    
    public function prepareDateForFilter($string)
    {
        return date("Y-m-d H:i:s", strtotime($string));
    }
    
    public function getOrdersData($filter)
    {
        $client = new SoapClient($this->remoteUrl . self::SERVICE_URL);
        $session = $client->login($this->apiUser, $this->apiKey);
        return $client->salesOrderList($session, $filter);    
    }
    
    public function showHelp()
    {
        if (isset($this->_args['h']) || isset($this->_args['help'])) {
            echo <<<USAGE
Usage:  php -f get_orders.php -- [options]

  --remoteUrl <url>             Remote Server Url
  --apiUser <user_name>         Soap User Name
  --apiKey <key>                Soap Key
  --from <date>                 'From' Order Filter
  --to <date>                   'To' Order Filter
  help                          This help

  <date>                        Should have format: YYYY-MM-DD or YYYY-MM-DD h:m:s
  
USAGE;
            die;
        }
    }
}

$shell = new Shell_Soap2();
$shell->run();